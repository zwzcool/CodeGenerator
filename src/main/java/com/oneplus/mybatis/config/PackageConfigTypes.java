package com.oneplus.mybatis.config;

import java.util.Set;

/**
 * Title: <br/>
 * <p>
 * Create Date: 2015/11/27 23:04 <br/>
 * Package Name: com.hujifang.mybatis.config <br/>
 * Company:  All Rights Reserved. <br/>
 * Copyright © 2015 <br/>
 * </p>
 * <p>
 * author: Hujifang <br/>
 * 1st_examiner: <br/>
 * 2nd_examiner: <br/>
 * </p>
 */
public class PackageConfigTypes {
    /**
     * 生成文件类型
     */
    private ConfigType type;

    Set<PackageConfigType> packageConfigTypeSet;

    public PackageConfigTypes() {
    }

    public PackageConfigTypes(ConfigType type, Set<PackageConfigType> packageConfigTypeSet) {
        this.type = type;
        this.packageConfigTypeSet = packageConfigTypeSet;
    }

    public ConfigType getType() {
        return type;
    }

    public void setType(ConfigType type) {
        this.type = type;
    }

    public Set<PackageConfigType> getPackageConfigTypeSet() {
        return packageConfigTypeSet;
    }

    public void setPackageConfigTypeSet(Set<PackageConfigType> packageConfigTypeSet) {
        this.packageConfigTypeSet = packageConfigTypeSet;
    }

    public enum ConfigType {
        MODEL("model"),MAPPER("mapper"),MAPPER_CONFIG("mapperConfig"),RESULT("result"),SERVICE("service"),CONTROLLER("controller");
        public String key;
        ConfigType(String key){
            this.key=key;
        }
    }
}
